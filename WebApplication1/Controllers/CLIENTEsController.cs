﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.Datos;

namespace WebApplication1.Controllers
{
    public class CLIENTEsController : ApiController
    {
        private fac0mxdbEntities db = new fac0mxdbEntities();

        public SqlDataReader rs;
        public SqlConnection myConnection;
        public SqlDataReader myReader;
        public String SQL = "";

        //escritura
        public SqlDataReader conex2(string qry)
        {
            SqlDataReader myReader = null;
            Configuration rootConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            if (0 < rootConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                connString = rootConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                if (null != connString)
                {
                    SqlConnection myConnection2 = new SqlConnection(connString.ConnectionString);
                    try
                    {
                        myConnection2.Open();
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            //myCommand.CommandTimeout = 5000;
                            myCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection2.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");
        }
        //lectura        
        public SqlDataReader conex(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection == null)
                        {
                            myConnection = new SqlConnection(connString.ConnectionString);
                            myConnection.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader != null)
                            {
                                myReader.Dispose();
                            }
                            myReader = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }



        // GET: api/CLIENTEs
        public IQueryable<CLIENTE> GetCLIENTES()
        {
            return db.CLIENTES;
        }

        // GET: api/CLIENTEs/5
        [ResponseType(typeof(CLIENTE))]
        public IHttpActionResult GetCLIENTE(string id)
        {
            int a = 0;
            int b = 0;

            SQL = "select TOTLLAREA,TOTNUMLLA,llave_unica,* from fac0mxdb..CLIENTES  where STATUS=1 AND NIP=" + id ;
            rs = conex(SQL);
            if (rs.HasRows)
            {
                rs.Read();
                id = rs[2].ToString();
                a = Convert.ToInt32(rs[1].ToString());
                b = Convert.ToInt32(rs[0].ToString());
            }

            if (a - b <= 0)
            {
                id = "XXX";
            }

            CLIENTE cLIENTE = db.CLIENTES.Find(id);
            if (cLIENTE == null)
            {
                return NotFound();
            }

            return Ok(cLIENTE);
        }

        // PUT: api/CLIENTEs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCLIENTE(string id, string user_id, CLIENTE cLIENTE)
        {

            int TOTLLAREA = 0;
            int TOTLLAREA2 = 0;
            int TOTLLAREA3 = 0;
            string nombre = "";
            int numero_sus = 0;
            int a=0;
            int b=0;

            string guid = Guid.NewGuid().ToString();


            SQL = "select TOTLLAREA,TOTNUMLLA,llave_unica,NOMSUS,NUMSUS,orderhdr_id,order_item_seq,* from fac0mxdb..CLIENTES  where status=1 and NIP=" + id;
            rs = conex(SQL);
            if (rs.HasRows)
            {
                rs.Read();
                //id = rs[2].ToString();
                nombre = rs[3].ToString();
                numero_sus = Convert.ToInt32(rs[4].ToString());
                TOTLLAREA = Convert.ToInt16(rs[0].ToString());
                a = Convert.ToInt32(rs[5].ToString());
                b = Convert.ToInt32(rs[6].ToString());
            }

            TOTLLAREA2 = Convert.ToInt16(cLIENTE.TOTLLAREA); //el numero que viene del servicio


            TOTLLAREA3 = TOTLLAREA + TOTLLAREA2;



            SQL = "INSERT INTO fac0mxdb..Registro_Llamada2 (nip_cliente,id_usuario,nombre_llama, num_consultas, califica_emp, tema, log_registro, guid,periodo,orderhdr_id,order_item_seq, depuradoBi) VAlUES " +
                  "(" + id + ",'" + user_id + "','" + nombre + "'," + TOTLLAREA2 + ",'',' " + Convert.ToString(cLIENTE.tema) + "',GETDATE(),'" + guid + "','', " + a + "," + b + ",1)";
            conex2(SQL);

            SQL = "UPDATE fac0mxdb..CLIENTES set TOTLLAREA=" + TOTLLAREA3 + "  where  Status=1 and NIP=" + id;
            conex2(SQL);


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            if (1 != 1)
            {
                return BadRequest();
            }
            /*
            if (id != cLIENTE.llave_unica)
            {
                return BadRequest();
            }*/

            //db.Entry(cLIENTE).State = EntityState.Modified;
            db.Entry(cLIENTE).State = EntityState.Detached;

            try
            {
                //db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CLIENTEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CLIENTEs
       /* [ResponseType(typeof(CLIENTE))]
        public IHttpActionResult PostCLIENTE(CLIENTE cLIENTE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CLIENTES.Add(cLIENTE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CLIENTEExists(cLIENTE.llave_unica))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cLIENTE.llave_unica }, cLIENTE);
        }

        // DELETE: api/CLIENTEs/5
        [ResponseType(typeof(CLIENTE))]
        public IHttpActionResult DeleteCLIENTE(string id)
        {
            CLIENTE cLIENTE = db.CLIENTES.Find(id);
            if (cLIENTE == null)
            {
                return NotFound();
            }

            db.CLIENTES.Remove(cLIENTE);
            db.SaveChanges();

            return Ok(cLIENTE);
        }*/

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CLIENTEExists(string id)
        {
            return db.CLIENTES.Count(e => e.llave_unica == id) > 0;
        }
    }
}